const Messages = require('./messages.json');
const Requests = require('./requests.js');

module.exports = {
    'help': function (msg) {
        return msg.reply(Messages['help']);
    },
    'events': function (msg) {
        return Requests['events'](msg, "beepboop", "dev")
    }
}