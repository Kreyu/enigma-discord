# Enigma Discord Bot

A DayZ game mod Discord bot. Made using [discord.js](https://discord.js.org/).

## Getting Started

Install packages

```
npm install
```

Rename *env.json.example* to *env.json* and insert Discord bot token.
After that, you can simply run the bot executing *bot.js*:

```
node bot.js
```
