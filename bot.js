const Discord = require('discord.js');
const Commands = require('./commands.js');
const Messages = require('./messages.json');
const Env = require('./env.json');
const client = new Discord.Client();
const request = require('request');

client.on('ready', () => {
    console.log(`Logged in as ${client.user.tag}!`);

    client.user.setPresence({ 
        status: 'online', 
        game: {
            name: 'Beep Boop - $help' 
        } 
    })
});

client.on('message', msg => {
    return checkPrefix(msg) ?
        parseCommand(msg) : false;
});

function checkPrefix(msg) {
    return msg.content.substring(0, 1) == "$";
}

function parseCommand(msg) {
    var command = msg.content.substring(1);

    if (typeof Commands[command] === "function") {
        return Commands[command](msg);
    } else {
        msg.reply(Messages.notFound);
    }
}

client.login(Env.token);
