const Messages = require('./messages.json');
const Env = require('./env.json');
const request = require('request');

function getHead(url) {
    return {
        url: url,
        json: true,
        headers: {'User-Agent': 'request'}
    }
}

module.exports = {
    "events": function (msg, server, token) {
        var url = Env.host + "/servers/" + server + "/events/recent?token=" + token;

        request.get(getHead(url), (err, res, data) => {
            if (err) console.log('Error: ', err);
            else if (res.statusCode !== 200) console.log('Status: ', res.statusCode);
            else {
                var events = data.data;

                if (events.length === 0) {
                    msg.reply(Messages.noEvents);
                }

                var message = Messages.eventsList[0] + events.length + Messages.eventsList[1];

                for (var i=0; i<events.length; i++) {
                    var event = events[i];
                    message += event.created_at + ' | ' + event.type.name + "\n";
                }

                msg.reply(message);
            }
        });        
    }
}